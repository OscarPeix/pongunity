﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelotilla : MonoBehaviour
{
      public float velocidad;
      private float posY;
      private float posX;
      public float maxX;
      public float maxY;


    // Start is called before the first frame update
   

    // Update is called once per frame
    void Update()
    {

        
        posY = transform.position.y + velocidad*Time.deltaTime;

         if(posY>maxY)
        {
            posY = maxY;
        }
            else if(posY<-maxY)
        {
            posY = -maxY;
        }

        posX = transform.position.x + velocidad*Time.deltaTime;

         if(posX>maxX)
        {
            posX = maxX;
        }
            else if(posX<-maxX)
        {
            posX = -maxX;
        }

        transform.position = new Vector3(posX, posY, transform.position.z);
        
    }
}
